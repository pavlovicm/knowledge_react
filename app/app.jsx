var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');
var Main = require('Main');
var Weather = require('Weather');
var About = require('About');
var Examples = require('Examples');
var WhyDecarbonise = require('WhyDecarbonise');
var Library = require('Library');
var Assess = require('Assess');

// load ionicons
// var Ionicon  = import Ionicon from 'react-ionicons';

// load bootstrap
require('style!css!bootstrap')



// load style css
require('style!css!AplicationStyle')


ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Main}>
      <Route path="about" component={About}/>
      <Route path="examples" component={Examples}/>
      <Route path="WhyDecarbonise" component={WhyDecarbonise}/>
      <Route path="Assess" component={Assess}/>
      <Route path="Library" component={Library}/>
      
      <IndexRoute component={Weather}/>
    </Route>
  </Router>,
  document.getElementById('app')
);
