var React = require('react');
var {Link, IndexLink} = require('react-router');

var Nav = React.createClass({
  render: function () {
    return (
      <div className="knw-navigation">
        <div  className="container">
          <div className="row knw-mob_nav_wrap">
            <div className="col-md-3 knw-logo_wrap">
              <IndexLink className="menu_logo" to="/" activeClassName="active" >Carbon</IndexLink>
            </div>
            <div className="knw-nav_desk">
              <ul className="nav">
                <li className="active menu_item"><Link to="/WhyDecarbonise" activeClassName="active">Why decarbonise?</Link></li>
                <li className="menu_item"><Link to="/Assess" activeClassName="active">Assess</Link></li>
                <li className="menu_item"><Link to="" activeClassName="active">Opportunities</Link></li>
                <li className="menu_item"><Link to="" activeClassName="active">Implement</Link></li>
                <li className="menu_item"><Link to="/Library" activeClassName="active">Library</Link></li>
                <li className="menu_item"><Link to="" activeClassName="active">About</Link></li>
                <li className="menu_item search_icon_hide">
                  
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = Nav;
