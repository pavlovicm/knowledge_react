module.exports = function(grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      less: {
        files: ['app/styles/less/**/*.less'],
        tasks: ['less:development']
      }
    },
    less: {
      development: {
        options: {
          sourceMap: true,
          sourceMapRootpath: '/',
          sourceMapBasepath: 'app'
        },
        files: {
          'app/styles/main.css': 'app/styles/less/style.less',
        }
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'css/',
          src: ['main.css'],
          dest: 'css/',
          ext: '.min.css'
        }]
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.registerTask('dev', [
    'less:development',
    'watch'
  ]);
  
};